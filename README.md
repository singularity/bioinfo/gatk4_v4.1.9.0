# Package gatk4 version 4.1.9.0
https://gatk.broadinstitute.org/hc/en-us/articles/360036194592-Getting-started-with-GATK4

GATK stands for Genome Analysis Toolkit. It is a collection of command-line tools for analyzing high-throughput sequencing data with a primary focus on variant discovery. The tools can be used individually or chained together into complete workflows.  

Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH gatk4 Version: 4.1.9.0
Singularity container based on the recipe: Singularity.gatk4_v4.1.9.0
## Local build:
```bash
sudo singularity build gatk4_v4.1.9.0.sif Singularity.gatk4_v4.1.9.0
```
## Get image help:

```bash
singularity run-help gatk4_v4.1.9.0.sif
```
### Default runscript: gatk4
## Usage:
```bash
./gatk4_v4.1.9.0.sif --help
```
or:
```bash
singularity exec gatk4_v4.1.9.0.sif gatk4 --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:

```bash
singularity pull gatk4_v4.1.9.0.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/gatk4_v4.1.9.0/gatk4_v4.1.9.0:latest
```